﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Fuzzy
{
    public static class Utils
    {
        public static double Max(params double[] values)
        {
            double res = double.MinValue;
            values.ToList().ForEach(v => res = Math.Max(v, res));
            return res;
        }

        public static double Min(params double[] values)
        {
            double res = double.MaxValue;
            values.ToList().ForEach(v => res = Math.Min(v, res));
            return res;
        }
    }

    public class Rule
    {
        public string Conclusion { get; set; }
        public Dictionary<string, string> Conditions { get; set; }

        public static List<Rule> ParseRules(string json)
        {
            var rules = new List<Rule>();

            var jsonObj = JsonConvert.DeserializeObject<JArray>(json);
            foreach(var r in jsonObj.Children<JObject>())
            {
                var conds = new Dictionary<string, string>();
                foreach(var c in r.Properties().First().Children<JObject>().Properties())
                {
                    conds.Add(c.Name, c.Value.ToString());
                }

                var rule = new Rule()
                {
                    Conclusion = r.Properties().First().Name,
                    Conditions = conds
                };

                rules.Add(rule);
            }

            return rules;
        }
    }

    public class Formula
    {
        public int A { get; set; }
        public int B { get; set; }
        public int C { get; set; }
        public int D { get; set; }

        public double Fuzzify(int input)
        {
            if (HaveOnlyRightPart())
            {
                if (input <= C) return 1;
                if (input >= D) return 0;
                return (double)(input - D)/(C - D);
            }
            else if (HaveOnlyLeftPart())
            {
                if (input <= A) return 0;
                if (input >= B) return 1;
                return (double)(input - A) / (B - A);
            }
            else
            {
                return Utils.Max(
                    Utils.Min(
                        (double)(input - A) / (B - A), 
                        1,
                        (double)(D - input) / (D - C)), 
                    0);
            }
        }
        public bool ContainsPoint(double x)
        {
            if (HaveOnlyRightPart())
            {
                return x < D;
            }
            else if (HaveOnlyLeftPart())
            {
                return x > A;
            }
            else
            {
                return x > A && x < D;
            }
        }

        private bool HaveOnlyRightPart() => A == B;
        private bool HaveOnlyLeftPart() => C == D;
    }

    public class Formulas : Dictionary<string, Formula>
    {
        public static Dictionary<string, Formulas> ParseFormulas(string json)
        {
            var allFormulas = new Dictionary<string, Formulas>();

            var jsonObj = JsonConvert.DeserializeObject<JObject>(json);
            foreach (var p1 in jsonObj.Properties())
            {
                var formulas = new Formulas();
                var name = p1.Name;

                foreach (var c in p1.Values<JObject>().First().Properties())
                {
                    var arr = c.Value.ToArray();

                    formulas.Add(c.Name, new Formula() {
                        A = arr[0].Value<int>(),
                        B = arr[1].Value<int>(),
                        C = arr[2].Value<int>(),
                        D = arr[3].Value<int>()
                    });
                }

                allFormulas.Add(name, formulas);
            }

            return allFormulas;
        }
    }

    public class FuzzyInference
    {
        private readonly Dictionary<string, string> questions;
        private readonly Dictionary<string, Formulas> formulas;
        private readonly List<Rule> rules;

        private Dictionary<string, int> inputs = new Dictionary<string, int>();
        private Dictionary<string, double> fuzzyfiedInputs = new Dictionary<string, double>();
        private Dictionary<string, double> ruleApplicated = new Dictionary<string, double>();

        public FuzzyInference(Dictionary<string, string> questions,
            Dictionary<string, Formulas> formulas, 
            List<Rule> rules)
        {
            this.questions = questions;
            this.formulas = formulas;
            this.rules = rules;
        }

        public string GetConclusion()
        {
            GetInputs();
            Fuzzyfication();
            RuleApplication();
            double mass = Defuzzyfication();

            return GetAnswer(mass);
        }

        private void GetInputs()
        {
            foreach(var r in rules)
            {
                foreach(var c in r.Conditions)
                {
                    while (!inputs.ContainsKey(c.Key))
                    {
                        Console.WriteLine(questions[c.Key]);
                        var answer = Console.ReadLine();
                        int outInt = 0;
                        if (int.TryParse(answer, out outInt))
                        {
                            inputs[c.Key] = outInt;
                        }
                        else
                        {
                            Console.WriteLine("Invalid answer. Try again...");
                        }
                    }
                }
            }
        }

        private void Fuzzyfication()
        {
            foreach(var fInput in inputs)
            {
                var fDict = formulas[fInput.Key];

                foreach(var f in fDict)
                {
                    fuzzyfiedInputs.Add(
                        fInput.Key + "." + f.Key,
                        f.Value.Fuzzify(fInput.Value));

                    Console.WriteLine(fuzzyfiedInputs.Last().Key + ":" 
                        + fuzzyfiedInputs.Last().Value);
                }
            }
        }

        private void RuleApplication()
        {
            foreach(var rule in rules)
            {
                var predicateValues = new List<double>();
                foreach(var predicate in rule.Conditions)
                {
                    predicateValues.Add(
                        fuzzyfiedInputs[predicate.Key + "." + predicate.Value]);
                }

                if (ruleApplicated.ContainsKey(rule.Conclusion))
                {
                    ruleApplicated[rule.Conclusion] = Utils.Max(
                        ruleApplicated[rule.Conclusion],
                        Utils.Min(predicateValues.ToArray()));
                }
                else
                {
                    ruleApplicated[rule.Conclusion] = Utils.Min(predicateValues.ToArray());
                }
            }
        }

        private double Defuzzyfication()
        {
            var resultFormulas = formulas.Last().Value;
            double numerator = 0, denominator = 0;

            for(int x = 1; x <= resultFormulas.Last().Value.D; x++)
            {
                double maxValue = double.MinValue;
                foreach(var f in resultFormulas)
                {
                    if (f.Value.ContainsPoint(x))
                    {
                        maxValue = Utils.Max(ruleApplicated[f.Key], maxValue);
                    }
                }
                numerator += x * maxValue;
                denominator += maxValue;
            }

            return numerator / denominator;
        }

        private string GetAnswer(double mass)
        {
            var resultFormulas = formulas.Last().Value;
            foreach (var f in resultFormulas)
            {
                if (f.Value.ContainsPoint(mass)) return f.Key;
            }

            return string.Empty;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var rulesText = File.ReadAllText("Rules.json");
            var rules = Rule.ParseRules(rulesText);

            var formulasText = File.ReadAllText("Formulas.json");
            var formulas = Formulas.ParseFormulas(formulasText);

            var questionsText = File.ReadAllText("Questions.json");
            var questions = JsonConvert.DeserializeObject<Dictionary<string, string>>(questionsText);

            while (true)
            {
                Console.WriteLine("Determine if Unity3d game engine suits for your new game.\n");

                var fuzzyInference = new FuzzyInference(questions, formulas, rules);
                var result = fuzzyInference.GetConclusion();

                Console.WriteLine($"Result: {result}");
                Console.WriteLine();
                Console.WriteLine($"Press ESC to exit, or any another to try again...");

                if(Console.ReadKey().Key == ConsoleKey.Escape) break;

                Console.Clear();
            }
        }
    }
}
