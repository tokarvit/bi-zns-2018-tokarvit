﻿using System;
using System.Collections.Generic;

namespace Indeterminateness
{
    public class Program
    {
        public const float FLOAT_INACCURACY = 0.00000001f;

        static void Main(string[] args)
        {
            string rulesPath;
            if(args.Length == 0)
            {
                rulesPath = "Rules.txt";
            }
            else
            {
                rulesPath = args[0];
            }

            while (true)
            {
                var knowledgeModule = new KnowledgeModule();
                var parser = new RuleParser(knowledgeModule);
                var rules = parser.ParseRules(rulesPath);

                var inference = new InferenceProcessor(rules, knowledgeModule);
                var conclusions = inference.Run();

                if(conclusions.Count == 0)
                {
                    Console.WriteLine("It's not possible to make conclusion based on your answers.");
                }
                else
                {
                    Console.WriteLine("The best game engines for you are:");
                    foreach (var c in conclusions)
                    {
                        Console.WriteLine("{0} - {1:N2}% - {2}",
                        c.Key, c.Value, knowledgeModule.Why[c.Key]);
                    }
                }

                Console.WriteLine();
                Console.WriteLine("Try again?");
                if (!Console.ReadLine().Contains("yes"))
                    break;

                Console.Clear();
            }
        }
    }
}
