﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Indeterminateness
{
    public class KnowledgeModule
    {
        public Dictionary<string, string> Questions { get; set; }
        public Dictionary<string, float> Probabilities { get; set; } = new Dictionary<string, float>();
        public List<string> FinalConclusions { get; set; }
        public List<string> MiddlelConclusions { get; set; } = new List<string>();
        public Dictionary<string, float> InitialProbabilities { get; set; }
        public Dictionary<string, string> Why { get; set; }

        public List<string> FactsInConclusions { get; set; } = new List<string>();

        public Dictionary<string, bool> UserBools { get; set; } = new Dictionary<string, bool>();
        public Dictionary<string, int> UserIntegers { get; set; } = new Dictionary<string, int>();

        public KnowledgeModule()
        {
            var jsonText = File.ReadAllText(@".\Questions.json");
            Questions = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonText);

            jsonText = File.ReadAllText(@".\Probabilities.json");
            InitialProbabilities = JsonConvert.DeserializeObject<Dictionary<string, float>>(jsonText);

            jsonText = File.ReadAllText(@".\MainConclusions.json");
            FinalConclusions = JsonConvert.DeserializeObject<List<string>>(jsonText);

            jsonText = File.ReadAllText(@".\Why.json");
            Why = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonText);
        }

        public bool AskBool(string name, string tryToProve)
        {
            if (!UserBools.ContainsKey(name))
            {
                Console.WriteLine(Questions[name] + " ('yes' or 'no') - trying to prove " + tryToProve);
                while (true) {
                    var answer = Console.ReadLine();
                    if (answer == "yes" || answer == "no")
                    {
                        UserBools[name] = answer == "yes";
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid answer. Try again...");
                    }
                }
            }

            return UserBools[name];
        }

        public int AskInt(string name, string tryToProve)
        {
            if (!UserIntegers.ContainsKey(name))
            {
                Console.WriteLine(Questions[name] + " (integer) - trying to prove " + tryToProve);
                while (true)
                {
                    var answer = Console.ReadLine();
                    int outInt = 0;
                    if (int.TryParse(answer, out outInt))
                    {
                        UserIntegers[name] = outInt;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid answer. Try again...");
                    }
                }
            }

            return UserIntegers[name];
        }
    }
}
