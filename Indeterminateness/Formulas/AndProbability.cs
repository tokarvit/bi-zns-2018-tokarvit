﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Indeterminateness
{
    public class AndProbability : IRule
    {
        public List<IVariable> Conditions { get; set; } = new List<IVariable>();
        public Variable Conclusion { get; set; }

        private KnowledgeModule _knowledgeModule;

        public AndProbability(KnowledgeModule knowledgeModule)
            => _knowledgeModule = knowledgeModule;

        public List<IVariable> GetDependencies()
            => Conditions.Where(v => !v.ExistAnswer()).ToList();

        public void Calculate()
        {
            float numerator = Conclusion.GetProbability();

            foreach (var c in Conditions)
            {
                numerator *= c.GetProbability(Conclusion.Name);
            }

            float denominator = 0;
            if (_knowledgeModule.FinalConclusions.Contains(Conclusion.ToString()))
            {
                foreach (var conc in _knowledgeModule.FinalConclusions)
                {
                    float multiplication = _knowledgeModule.InitialProbabilities[conc];
                    foreach (var cond in Conditions)
                    {
                        multiplication *= cond.GetProbability(conc);
                    }

                    denominator += multiplication;
                }

                if (denominator < Program.FLOAT_INACCURACY)
                {
                    return;
                }
                
                float result = numerator / denominator;

                if (result > Program.FLOAT_INACCURACY)
                {
                    _knowledgeModule.FactsInConclusions.Remove(Conclusion.Name);
                    _knowledgeModule.Probabilities[Conclusion.Name] = result;
                }
            }
            else
            {
                if (numerator > Program.FLOAT_INACCURACY)
                {
                    _knowledgeModule.FactsInConclusions.Remove(Conclusion.Name);
                    _knowledgeModule.Probabilities[Conclusion.Name] = numerator;
                }
            }

            
        }

        //public void Calculate1()
        //{
        //    float numerator = _knowledgeModule.FinalConclusions.Contains(Conclusion.ToString()) 
        //        ? Conclusion.GetProbability()
        //        : 1;
        //    foreach (var c in Conditions) {
        //        numerator *= c.GetProbability(Conclusion.Name);
        //    }
            
        //    if (numerator < Program.FLOAT_INACCURACY)
        //    {
        //        _knowledgeModule.Probabilities[Conclusion.Name] = 0;
        //        return;
        //    }

        //    float denominator = 0;
        //    if (_knowledgeModule.FinalConclusions.Contains(Conclusion.ToString()))
        //    {
        //        foreach (var conc in _knowledgeModule.FinalConclusions)
        //        {
        //            float multiplication = _knowledgeModule.GetProbability(conc);
        //            foreach (var cond in Conditions)
        //            {
        //                multiplication *= cond.GetProbability(conc);
        //            }

        //            denominator += multiplication;
        //        }
        //    }
        //    else
        //    {
        //        float multiplication = 1;
        //        foreach (var cond in Conditions)
        //        {
        //            multiplication *= cond.GetProbability();
        //        }
        //        denominator += multiplication;
        //    }

        //    if (denominator < Program.FLOAT_INACCURACY) {
        //        _knowledgeModule.Probabilities[Conclusion.Name] = 0;
        //        return;
        //    }
            

        //    float result = numerator / denominator;

        //    if(result > Program.FLOAT_INACCURACY)
        //    {
        //        _knowledgeModule.Probabilities[Conclusion.Name] = result;
        //    }

        //    if (result < Program.FLOAT_INACCURACY)
        //    {
        //        _knowledgeModule.Probabilities[Conclusion.Name] = 0;
        //    }
        //}

    }
}
