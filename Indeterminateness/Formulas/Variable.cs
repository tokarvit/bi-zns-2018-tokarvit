﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Indeterminateness
{
    public class Variable : IVariable
    {
        public string Name { get; set; }
        public bool IsTrue { get; set; } = true;
        public string TryToProve { get; set; }

        private KnowledgeModule _knowledgeModule; 

        public Variable(KnowledgeModule knowledgeModule)
            => _knowledgeModule = knowledgeModule;

        public float GetProbability()
        {
            float prob;
            if (_knowledgeModule.MiddlelConclusions.Contains(Name))
            {
                prob = _knowledgeModule.InitialProbabilities[Name]
                    * (_knowledgeModule.Probabilities.ContainsKey(Name) ? _knowledgeModule.Probabilities[Name] : 1);
            }
            else if (!_knowledgeModule.FinalConclusions.Contains(Name))
            {
                prob = _knowledgeModule.AskBool(Name, TryToProve) ? 1 : 0;
            }
            else
            {
                prob = _knowledgeModule.InitialProbabilities[Name];
            }

            return IsTrue
                ? prob
                : 1 - prob;
        }

        public float GetProbability(string conditionalVariable)
        {
            float prob;
            if (_knowledgeModule.MiddlelConclusions.Contains(Name))
            {
                if (_knowledgeModule.InitialProbabilities.ContainsKey(Name + "|" + conditionalVariable))
                    prob = _knowledgeModule.InitialProbabilities[Name + "|" + conditionalVariable]
                        * _knowledgeModule.InitialProbabilities[Name]
                        * (_knowledgeModule.Probabilities.ContainsKey(Name) ? _knowledgeModule.Probabilities[Name] : 1);
                else
                    prob = 0;
            }
            else if (!_knowledgeModule.FinalConclusions.Contains(Name))
            {
                prob = _knowledgeModule.AskBool(Name, TryToProve) ? 1 : 0;
            }
            else
            {
                prob = _knowledgeModule.InitialProbabilities[Name];
            }

            return IsTrue
                ? prob
                : 1 - prob;
        }

        public bool ExistAnswer()
        {
            return _knowledgeModule.Probabilities.ContainsKey(Name);
        }

        public override string ToString() => Name;
    }
}
