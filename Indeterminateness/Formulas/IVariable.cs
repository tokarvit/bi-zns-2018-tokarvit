﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Indeterminateness
{
    public interface IVariable
    {
        string Name { get; set; }
        string TryToProve { get; set; }

        float GetProbability();

        float GetProbability(string conditionalVariable);

        bool ExistAnswer();
    }
}
