﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Indeterminateness
{
    public class CompareIntVariable : IVariable
    {
        public string Name { get; set; }
        public string VariableName { get; set; }
        public string CompareOperator { get; set; }
        public string TryToProve { get; set; }

        private int intToCompare;
        private KnowledgeModule _knowledgeModule;

        public CompareIntVariable(string intToCompareStr, string variableName, 
            string compareOperator, KnowledgeModule knowledgeModule)
        {
            this.VariableName = variableName;
            this.CompareOperator = compareOperator;
            this._knowledgeModule = knowledgeModule;

            int intWeKnow = 0;
            if (int.TryParse(intToCompareStr, out intWeKnow))
            {
                intToCompare = intWeKnow;
            }
            else
            {
                throw new Exception("Parsing CompareIntFormula failed. " + intToCompareStr + ", " + Name);
            }

            Name = $"({intToCompare} {CompareOperator} {VariableName})";
        }
        
        public bool Evaluate(int valueToCompare)
        {
            switch (CompareOperator)
            {
                case "<": return intToCompare < valueToCompare;
                case "<=": return intToCompare <= valueToCompare;
                case ">": return intToCompare > valueToCompare;
                case ">=": return intToCompare >= valueToCompare;
                case "=": return intToCompare == valueToCompare;
                case "!=": return intToCompare != valueToCompare;
                default: throw new InvalidOperationException(
                    "Comparing operator '" + CompareOperator + "' is not supported");
            }
        }

        public float GetProbability()
        {
            float prob;
            prob = Evaluate(_knowledgeModule.AskInt(VariableName, TryToProve)) ? 1 : 0;

            return prob;
        }

        public float GetProbability(string conditionalVariable)
        {
            float prob;
            prob = Evaluate(_knowledgeModule.AskInt(VariableName, TryToProve)) ? 1 : 0;
            prob *= _knowledgeModule.InitialProbabilities[Name + "|" + conditionalVariable];

            return prob;
        }

        public bool ExistAnswer()
        {
            return _knowledgeModule.UserIntegers.ContainsKey(VariableName);
        }

        public override string ToString() => Name;
    }
}
