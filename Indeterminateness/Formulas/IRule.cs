﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Indeterminateness
{
    public interface IRule
    {
        List<IVariable> Conditions { get; set; }
        Variable Conclusion { get; set; }

        List<IVariable> GetDependencies();

        void Calculate();
    }
}
