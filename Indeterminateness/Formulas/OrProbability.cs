﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Indeterminateness
{
    public class OrProbability : IRule
    {
        public List<IVariable> Conditions { get; set; } = new List<IVariable>();
        public Variable Conclusion { get; set; }

        private KnowledgeModule _knowledgeModule;

        public OrProbability(KnowledgeModule knowledgeModule)
            => _knowledgeModule = knowledgeModule;

        public List<IVariable> GetDependencies()
            => Conditions.Where(v => !v.ExistAnswer()).ToList();

        public void Calculate()
        {
            float probability = 0;
            foreach (var cond in Conditions)
            {
                probability = Math.Max(probability, cond.GetProbability());
            }

            if (probability > Program.FLOAT_INACCURACY)
            {
                _knowledgeModule.Probabilities[Conclusion.Name] = probability;
                return;
            }
            _knowledgeModule.Probabilities[Conclusion.Name] = 0;
            return;
        }

    }
}