﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Indeterminateness
{
    public class RuleParser
    {
        private readonly string[] binaryOperators = 
            new string[] { "||", "&&", "<", ">", "<=", ">=", "=", "!=" };
        private readonly string[] comparingOperators =
            new string[] { "<", ">", "<=", ">=", "=", "!=" };
        private readonly KnowledgeModule _knowledgeModule;

        public RuleParser(KnowledgeModule knowledgeModule)
        {
            this._knowledgeModule = knowledgeModule;
        }

        public List<IRule> ParseRules(string fileName)
        {
            var rules = new List<IRule>();
            var lines = File.ReadAllLines(fileName);

            foreach(var l in lines)
            {
                if(!string.IsNullOrWhiteSpace(l))
                    rules.Add(ParseRule(l));
            }

            return rules;
        }
        
        private IRule ParseRule(string line)
        {
            var condAndConc = line.Replace("IF ", "").Split(" THEN");
            var conditionWithOperands = condAndConc.First().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            conditionWithOperands.ToList().ForEach(s => s.Trim());

            IRule rule;
            if (conditionWithOperands.Contains("||"))
            {
                rule = new OrProbability(_knowledgeModule);
            }
            else
            {
                rule = new AndProbability(_knowledgeModule);
            }
            rule.Conclusion = new Variable(_knowledgeModule) { Name = condAndConc.Last().Trim() };
            _knowledgeModule.MiddlelConclusions.Add(rule.Conclusion.Name);

            for (int i = 0; i < conditionWithOperands.Length; i++ )
            {
                if (binaryOperators.Contains(conditionWithOperands[i]))
                {
                    continue;
                }
                else if(i + 1 < conditionWithOperands.Length
                    && IsCompareFormula(conditionWithOperands[i + 1]))
                {
                    rule.Conditions.Add(new CompareIntVariable(conditionWithOperands[i], conditionWithOperands[i + 2], 
                        conditionWithOperands[i + 1], _knowledgeModule)
                    {
                        TryToProve = rule.Conclusion.Name
                    });
                    i += 2;
                }
                else
                {
                    rule.Conditions.Add(new Variable(_knowledgeModule)
                    {
                        Name = conditionWithOperands[i].Replace("!", ""),
                        TryToProve = rule.Conclusion.Name,
                        IsTrue = !conditionWithOperands[i].Contains("!")
                    });
                }
            }

            return rule;
        }
        
        private bool IsCompareFormula(string oper) => comparingOperators.Contains(oper);
    }
}
