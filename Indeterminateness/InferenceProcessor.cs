﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Indeterminateness
{
    public class InferenceProcessor
    {
        private readonly KnowledgeModule _knowledgeModule;

        private List<IRule> rules;

        public InferenceProcessor(List<IRule> rules, 
            KnowledgeModule knowledgeModule)
        {
            this.rules = rules;
            _knowledgeModule = knowledgeModule;

            _knowledgeModule.FactsInConclusions = rules
                .Select(f => f.Conclusion.Name)
                .Where(f => _knowledgeModule.MiddlelConclusions.Contains(f))
                .ToList();
        }

        public Dictionary<string, float> Run()
        {
            while (rules.Count > 0)
            {
                var dependencyOfRules = rules.Select(r => new { Count = r.GetDependencies().Count, Key = r })
                    .Where(e => e.Key.GetDependencies().Select(d => d.Name).Where(d => _knowledgeModule.FactsInConclusions.Contains(d)).Count() == 0)
                    .OrderBy(e => e.Count)
                    .ToDictionary(e => e.Key, e => e.Count);

                if (dependencyOfRules.Count == 0) break;

                rules.Remove(dependencyOfRules.First().Key);
                dependencyOfRules.First().Key.Calculate();
            }

            var result = _knowledgeModule.Probabilities
                .Where(p => _knowledgeModule.FinalConclusions.Contains(p.Key))
                .Where(p => p.Value > 0)
                .OrderByDescending(e => e.Value)
                .ToDictionary(e => e.Key, e => e.Value);

            var normal = new Dictionary<string, float>();
            foreach (var k in result.Keys)
            {
                float sum = 0;
                foreach (var j in result)
                {
                    sum += j.Value;              
                }
                normal[k] = result[k] / sum * 100;
            }

            return normal;
        }

    }
}
