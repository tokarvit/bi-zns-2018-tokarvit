﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inference
{
    public class KnowledgeModule
    {
        public Dictionary<string, object> knowledgeKeyValues = 
            new Dictionary<string, object>(); 

        public bool AskBool(string name)
        {
            if (!knowledgeKeyValues.ContainsKey(name))
            {
                Console.WriteLine("Is " + name + " true? ('yes' or 'no')");
                while (true) {
                    var answer = Console.ReadLine();
                    if (answer == "yes" || answer == "no")
                    {
                        knowledgeKeyValues[name] = answer == "yes";
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid answer.");
                    }
                }
            }

            return (bool)knowledgeKeyValues[name];
        }

        public int AskInt(string name)
        {
            if (!knowledgeKeyValues.ContainsKey(name))
            {
                Console.WriteLine("Answer on " + name + " with integer:");
                while (true)
                {
                    var answer = Console.ReadLine();
                    int outInt = 0;
                    if (int.TryParse(answer, out outInt))
                    {
                        knowledgeKeyValues[name] = outInt;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Invalid answer.");
                    }
                }
            }

            return (int)knowledgeKeyValues[name];
        }

        public void AddRange(IEnumerable<string> facts)
        {
            var dic = facts
                .Where(f => !knowledgeKeyValues.ContainsKey(f))
                .ToDictionary(k => k, v => (object)true);
            knowledgeKeyValues = knowledgeKeyValues.Concat(dic)
                .ToDictionary(x => x.Key, x => x.Value);
        }
    }
}
