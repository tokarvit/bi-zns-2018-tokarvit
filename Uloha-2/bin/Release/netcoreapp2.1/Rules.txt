﻿IF WithoutLinuxEditor && WithoutMacEditor THEN OnlyWindowsEditor
IF 0 = ExperienceInProgramming THEN VisualScripting 
IF 1 = ExperienceInProgramming && PreferLua THEN Lua
IF 1 = ExperienceInProgramming && PreferJS THEN JS
IF 1 = ExperienceInProgramming && !PreferLua && !PreferJS THEN JS && Lua
IF 1 < ExperienceInProgramming && 3 > ExperienceInProgramming THEN OwnLanguage && C#
IF 3 <= ExperienceInProgramming THEN C++ && C#
IF 3 > TeamMembers THEN SmallProject
IF 3 <= TeamMembers THEN BigProject
If SmallProject || BigProject THEN AnyProjectSize
IF AllowPaidVer || SmallProject THEN FreeForSmallProject
IF AllowPaidVer && BigProject THEN PaidForBigProject
IF FreeForSmallProject || PaidForBigProject THEN PaidButFreeForSmallProject
IF OwnLanguage || C# || C++ THEN OwnOrCTypeLanguage
IF OwnLanguage || VisualScripting THEN OwnLanguageOrVisualScripting
IF C++ || VisualScripting THEN CPlusPlusOrVisualScripting
IF C# && PaidButFreeForSmallProject && AnyProjectSize && WithoutLinuxEditor THEN Unity3d
IF CPlusPlusOrVisualScripting && PaidForBigProject && BigProject THEN UnrealEngine
IF Lua && AnyProjectSize THEN Defold
IF OwnLanguageOrVisualScripting && AllowPaidVer && SmallProject && OnlyWindowsEditor THEN GameMaker
IF JS && SmallProject THEN PixiJS
IF OwnOrCTypeLanguage && SmallProject && !WantExperienceForJob THEN Godot
IF C++ && AllowPaidVer && BigProject && OnlyWindowsEditor THEN CryEngine