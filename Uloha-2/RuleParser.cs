﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Inference
{
    public class RuleParser
    {
        private readonly string[] binaryOperators = 
            new string[] { "||", "&&", "<", ">", "<=", ">=", "=", "!=" };
        private readonly string[] comparingOperators =
            new string[] { "<", ">", "<=", ">=", "=", "!=" };
        private readonly KnowledgeModule knowledgeModule;

        public RuleParser(KnowledgeModule knowledgeModule)
        {
            this.knowledgeModule = knowledgeModule;
        }

        public List<Rule> ParseRules(string fileName)
        {
            var rules = new List<Rule>();
            var lines = File.ReadAllLines(fileName);

            foreach(var l in lines)
            {
                var condAndResult = l.Replace("IF ", "").Split(" THEN");
                var condition = ParseConditionForRuleWithoutBrackets(condAndResult.First());
                var result = ParseResult(condAndResult.Last());

                rules.Add(new Rule(condition, result));
            }

            return rules;
        }

        private List<BoolFormula> ParseResult(string resultStr)
        {
            var result = new List<BoolFormula>();

            resultStr.Split("&&", StringSplitOptions.RemoveEmptyEntries)
                .ToList()
                .ForEach(v => result.Add(
                    new BoolFormula(v.Trim(), knowledgeModule)));

            return result;
        }

        //private IFormula ParseConditionForRule(string conditionStr)
        //{
        //    //ParseConditionForRule(conditionStr.Split(' ', StringSplitOptions.RemoveEmptyEntries));
        //    int openedBracket = 0;
        //    int firstOpenedBracketIndex = -1;

        //    for(int i = 0; i < conditionStr.Length; i++)
        //    {
        //        char ch = conditionStr[i];
        //        if(ch == '(')
        //        {
        //            if (openedBracket == 0) firstOpenedBracketIndex = i;
        //            openedBracket++;
        //        }
        //        else if(ch == ')')
        //        {
        //            if(openedBracket == 1)
        //            {
        //                // todo 
        //            }


        //        }
        //    }
        //}

        private IFormula ParseConditionForRuleWithoutBrackets(string conditionStr)
            => ParseConditionForRuleWithoutBrackets(conditionStr.Split(' ', StringSplitOptions.RemoveEmptyEntries));

        //private IFormula ParseConditionForRuleWithoutBrackets(string[] conditionsElem)
        //{
        //    var left = new BoolFormula(conditionsElem[0]);

        //    if (conditionsElem.Length >= 3)
        //    {
        //        var next = conditionsElem[1];

        //        if (IsBinaryFormula(next))
        //        {
        //            var right = ParseConditionForRuleWithoutBrackets(conditionsElem
        //                .TakeLast(conditionsElem.Length - 2)
        //                .Aggregate((a,b) => a + b));

        //            return CreateBinaryFormula(next, left, right);
        //        }
        //    }

        //    return left;
        //}

        private IFormula ParseConditionForRuleWithoutBrackets(string[] conditionsElem, IFormula prevFormula = null)
        {
            IFormula right;
            if (prevFormula == null)
            {
                right = new BoolFormula(conditionsElem[conditionsElem.Length - 1], knowledgeModule);
                conditionsElem = conditionsElem.Take(conditionsElem.Length - 1).ToArray();
            }
            else
            {
                right = prevFormula;
            }

            if (conditionsElem.Length >= 2)
            {
                var next = conditionsElem[conditionsElem.Length - 1];

                if (IsBinaryFormula(next))
                {
                    IFormula left;
                    if (IsCompareFormula(next))
                    {
                        var compFormula = new CompareIntFormula(conditionsElem[conditionsElem.Length - 2], next, right, knowledgeModule);
                        if (conditionsElem.Length >= 3)
                        {
                            return ParseConditionForRuleWithoutBrackets(conditionsElem
                                .Take(conditionsElem.Length - 2).ToArray(), compFormula);
                        }
                        else
                        {
                            return compFormula;
                        }
                    }
                    else
                    {
                        left = ParseConditionForRuleWithoutBrackets(conditionsElem
                               .Take(conditionsElem.Length - 1).ToArray());
                        return CreateBinaryFormula(next, left, right);
                    }
                }
            }

            return right;
        }

        private bool IsBinaryFormula(string oper)
            => binaryOperators.Contains(oper);

        private bool IsCompareFormula(string oper)
            => comparingOperators.Contains(oper);

        private IFormula CreateBinaryFormula(string oper, IFormula left, IFormula right)
        {
            if (comparingOperators.Contains(oper))
            {
                
            }

            switch (oper)
            {
                case "||": return new OrFormula(left, right);
                case "&&": return new AndFormula(left, right);
                default: throw new InvalidOperationException(
                    "Logic operator '" + oper + "' doesn't exist");
            }
        }
    }
}
