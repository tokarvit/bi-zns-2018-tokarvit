﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inference
{
    public class IntFormula : IFormula
    {
        public string Name { get; private set; }

        private int? value;

        private KnowledgeModule knowledgeModule;

        public IntFormula(string name, KnowledgeModule knowledgeModule)
        {
            Name = name;
            this.knowledgeModule = knowledgeModule;
        }

        public int GetValue()
        {
            if (!value.HasValue)
                value = knowledgeModule.AskInt(Name);
            return value.Value;
        }

        public bool CanEvaluate()
        {
            return value.HasValue
                || knowledgeModule.knowledgeKeyValues.ContainsKey(Name);
        }

        public bool Evaluate() => true;

        public IEnumerable<string> Dependencies()
        {
            if (!knowledgeModule.knowledgeKeyValues.ContainsKey(Name))
                return new List<string>() { Name };

            return new List<string>();
        }

        public override string ToString() => Name;
    }
}
