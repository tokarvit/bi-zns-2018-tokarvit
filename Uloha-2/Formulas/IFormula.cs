﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inference
{
    public interface IFormula
    {
        string Name { get; }
        IEnumerable<string> Dependencies();
        bool CanEvaluate();
        bool Evaluate();
    }
}
