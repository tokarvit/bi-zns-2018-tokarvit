﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inference
{
    public class CompareIntFormula : IBinaryFormula
    {
        public string Name { get; private set; }

        private IntFormula intFormula;
        private int intToCompare;
        private string compareOperator;
        
        public CompareIntFormula(string intToCompareStr, string compareOperator, IFormula intFormula, KnowledgeModule knowledgeModule)
        {
            this.compareOperator = compareOperator;

            int intWeKnow = 0;
            if (int.TryParse(intToCompareStr, out intWeKnow))
            {
                this.intFormula = new IntFormula(intFormula.Name, knowledgeModule);
                intToCompare = intWeKnow;
            }
            else
            {
                throw new Exception("Parsing CompareIntFormula failed. " + intToCompareStr + ", " + intFormula.Name);
            }

            Name = $"({intToCompare} {compareOperator} {intFormula})";
        }

        public bool CanEvaluate() => intFormula.CanEvaluate();

        public bool Evaluate()
        {
            int valueToCompare = intFormula.GetValue();                

            switch (compareOperator)
            {
                case "<": return intToCompare < valueToCompare;
                case "<=": return intToCompare <= valueToCompare;
                case ">": return intToCompare > valueToCompare;
                case ">=": return intToCompare >= valueToCompare;
                case "=": return intToCompare == valueToCompare;
                case "!=": return intToCompare != valueToCompare;
                default: throw new InvalidOperationException(
                    "Comparing operator '" + compareOperator + "' is not supported");
            }
        }

        public IEnumerable<string> Dependencies() => intFormula.Dependencies();

        public override string ToString() => Name;
    }
}
