﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inference
{
    public class BoolFormula : IFormula
    {
        public string Name { get; private set; }

        private bool? value;
        private bool negation;

        private KnowledgeModule knowledgeModule;

        public BoolFormula(string name, KnowledgeModule knowledgeModule)
        {
            if(name[0] == '!')
            {
                Name = name.Replace("!", "");
                negation = true;
            }
            else
            {
                Name = name;
            }
            this.knowledgeModule = knowledgeModule;
        }

        public bool CanEvaluate()
        {
            return value.HasValue 
                || knowledgeModule.knowledgeKeyValues.ContainsKey(Name);
        }

        public bool Evaluate()
        {
            if (!value.HasValue)
                value = knowledgeModule.AskBool(Name);
            if (negation) value = !value;
            return value.Value;
        }

        public IEnumerable<string> Dependencies()
        { 
            if(!knowledgeModule.knowledgeKeyValues.ContainsKey(Name))
                return new List<string>() { Name };

            return new List<string>();
        }

        public override string ToString() => negation ? "!" + Name : Name;
    }
}
