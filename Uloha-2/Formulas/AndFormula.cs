﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inference
{
    public class AndFormula : IBinaryFormula
    {
        public string Name { get; private set; }

        private IFormula left;
        private IFormula right;

        public AndFormula(IFormula left, IFormula right)
        {
            this.left = left;
            this.right = right;
            Name = $"({left} && {right})";
        }

        public bool CanEvaluate()
            =>  left.CanEvaluate() && right.CanEvaluate();

        public bool Evaluate() => left.Evaluate() && right.Evaluate();

        public IEnumerable<string> Dependencies()
            => left.Dependencies().Union(right.Dependencies());

        public override string ToString() => Name;
    }
}
