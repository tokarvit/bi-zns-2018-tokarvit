﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inference
{
    public class OrFormula : IBinaryFormula
    {
        public string Name { get; private set; }

        private IFormula left;
        private IFormula right;

        public OrFormula(IFormula left, IFormula right)
        {
            this.left = left;
            this.right = right;
            Name = $"({left} || {right})";
        }

        public bool CanEvaluate()
        {
            if (left.CanEvaluate() && left.Evaluate())
                return true;

            if (right.CanEvaluate() && right.Evaluate())
                return true;

            return false;
        }

        public bool Evaluate() {
            if (left.CanEvaluate())
            {
                if (left.Evaluate()) return true;
            }

            if (right.CanEvaluate())
            {
                if (right.Evaluate()) return true;
            }

            return left.Evaluate() || right.Evaluate();
        }

        public IEnumerable<string> Dependencies()
            => left.Dependencies().Union(right.Dependencies());

        public override string ToString() => Name;
    }
}
