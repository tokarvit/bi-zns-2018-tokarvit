﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inference
{
    public class InferenceProcessor
    {
        private readonly KnowledgeModule _knowledgeModule;
        private readonly List<string> estimatedConclusions;

        private List<Rule> rules;
        private HashSet<string> factsInConclusions;

        public InferenceProcessor(List<Rule> rules, List<string> estimatedConclusions, 
            KnowledgeModule knowledgeModule)
        {
            this.rules = rules;
            this.estimatedConclusions = estimatedConclusions;
            _knowledgeModule = knowledgeModule;

            factsInConclusions = rules
                .SelectMany(r => r.Conclusion)
                .Select(f => f.Name)
                .ToHashSet();
        }

        public string Run()
        {
            while (rules.Count > 0)
            {
                var minDependencyRule = rules.Where(r => r.Condition.CanEvaluate())
                        .FirstOrDefault();
                
                if (minDependencyRule == null)
                {
                    var independedRules = MinusRulesRequiredConclusion();
                    if (independedRules.Count == 0) break;

                    minDependencyRule = independedRules.Aggregate((m, n)
                        => n.Dependencies().Count() < m.Dependencies().Count() ? n : m);
                }

                rules.Remove(minDependencyRule);
                var newFacts = minDependencyRule.Evaluate();
                _knowledgeModule.AddRange(newFacts);
                factsInConclusions = factsInConclusions.Where(f =>
                    !newFacts.Contains(f)).ToHashSet();

                var conclusions = newFacts.Where(f => estimatedConclusions.Contains(f));
                if (conclusions.Any())
                {
                    return "You can try to use " + conclusions.First() 
                        + ". Because of " + minDependencyRule.Condition.Name;
                }
            }

            return "It's not possible to make conclusion based on your answers. Please try again.";
        }

        /// <summary>
        /// Select rules except those which has in condition formulas from conclusion
        /// </summary>
        private List<Rule> MinusRulesRequiredConclusion()
        {

            return rules.Where(r => 
                        !r.Condition.Dependencies().Where(d => 
                            factsInConclusions.Contains(d)).Any())
                    .ToList();
        }
    }
}
