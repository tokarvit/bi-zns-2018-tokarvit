﻿using System;
using System.Collections.Generic;

namespace Inference
{
    class Program
    {
        static void Main(string[] args)
        {
            string rulesPath;
            if(args.Length == 0)
            {
                rulesPath = "Rules.txt";
            }
            else
            {
                rulesPath = args[0];
            }

            while (true)
            {
                var knowledgeModule = new KnowledgeModule();
                var parser = new RuleParser(knowledgeModule);
                var rules = parser.ParseRules(rulesPath);
                var estimatedConclusion = new List<string>()
                {
                    "Unity3d", "Defold", "UnrealEngine", "GameMaker","PixiJS","Godot","CryEngine"
                };
                var inference = new InferenceProcessor(rules, estimatedConclusion, knowledgeModule);
                var conclusion = inference.Run();

                Console.WriteLine(conclusion);
                Console.WriteLine("Try again?");
                if (!Console.ReadLine().Contains("yes"))
                    break;

                Console.Clear();
            }
        }
    }
}
