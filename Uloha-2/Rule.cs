﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inference
{
    public class Rule
    {
        public IFormula Condition { get; private set; }
        public List<BoolFormula> Conclusion { get; private set; }

        public Rule(IFormula condition, List<BoolFormula> result)
        {
            Condition = condition;
            Conclusion = result;
        }

        public IEnumerable<string> Dependencies() => Condition.Dependencies();

        public IEnumerable<string> Evaluate()
        {
            Console.WriteLine("Trying to prove " + Conclusion.Select(r => r.Name).Aggregate((i, j) => i + ", " + j));

            if (Condition.Evaluate())
                return Conclusion.Select(r => r.Name);

            return new List<string>();
        }
    }
}
